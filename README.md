Repositorio pro projeto pra salvar tudo junto

Atividades:
- Ver os residuos perto do sitivo ativo (ASP-98 pro sert e ASP-79 pro dat)

- Fazer docking e redocking com a R-fluoxicetina

- Docking feitos: 2, 3 e 9
- Docking faltam: 4, 5 e 7

- Adicionar instrucoes pra sobreposicao

Observacões:

- Pra ver os resultados, entrar na pasta /resultados-glg dentro da pasta /resultados e abrir com um visualizador de arquivos .txt

- A pasta do docking deve conter os arquivos .pdbqt, .gpf e .dpf para funcionar

- Não colocar caracteres especiais no nome do arquivo, como acentos e ponto porque pode bugar o programa

