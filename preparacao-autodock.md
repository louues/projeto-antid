Criar pasta para colocar todos as arquivos do docking
```
- File -> Preferences -> Set -> Startup Directory - Selecionar pasta -> Make Default
```

Preparar Proteina
```
1 - File -> Read Molecule -> Selecionar proteina
2 - Edit -> Delete Water
3 - Edit -> Hydrogens -> Add -> All Hydrogens -> Ok
4 - Edit -> Hydrogens -> Merge Non-Polar
5 - Edit -> Charges -> Add Kollman Charges
6 - Edit -> Atoms -> Assign AD4 type
7 - Grid -> Macromolecule -> Choose -> Seleciona a proteina -> Salvar como .pdbqt
```

Preparar Ligante
```
1 - Ligand -> Input -> Open -> Selecionar Ligante e Selecionar o file type .mol2
2 - Ligand -> Torsion Tree -> Detect Root
3 - Ligand -> Torsion Tree -> Choose Torsions -> Make all active bonds non-rotatable -> Make all rotatable bonds rotatable -> Done
4 - Ligand -> Output -> Save as PDBQT
```

- Deletar a proteina e molecula
- Abrir a nova proteina e molecula .pdbqt


Grid Box
```
1 - Grid -> Macromolecule -> Choose -> Selecionar proteina -> Yes
2 - Grid -> Set Map Types -> Directly -> Accept
3 - Grid -> Grid Box -> Configurar Grid Box -> File -> Close saving current
4 - Grid -> Output -> Save GPF -> Salvar como grid.gpf
```

Autogrid
```
cd path/to/pasta-do-docking
autogrid4 -p grid.gpf -l grid.glg
```

Parametros
```
1 - Docking -> Macromolecule -> Set Rigid Filename -> Selecionar Proteina
2 - Docking -> Ligand -> Choose -> Selecionar Ligante -> Accept
3 - Docking -> Search Parameters -> Genetic Algorithm -> Number of GA Runs: 100, Population Size: 150, Maximum Number of evals: long -> Accept
4 - Docking -> Docking Parameters -> Use defaults -> Accept
5 - Docking -> Output -> Lamarckian -> Salvar como dock.dpf
```

Autodock
```
cd path/to/pasta-do-docking
autodock4 -p dock.dpf -l dock.glg
```

